x0 = [-pi, 1e-3, -2];

global n theta
n = 2.1;
theta = -asin(2/n);

options = odeset('Events', @pevent);
[t, x, te, xe, ie] = ode45(@func, [0, 5], x0, options);

n = 0;
[t1, x1] = ode45(@func, [0 6], xe);


plot(t, x(:,1))
hold on
plot(t(end)+t1, x1(:,1))

% options = odeset('Events', @pevent);
% [t1, x1, te1, xe1, ie1] = ode45(@func, [0 6], xe, options);
