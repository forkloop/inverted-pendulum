function xPrime = func(t, x)
global n

xPrime = zeros(3, 1);
xPrime(1) = x(2);
xPrime(2) = sin(x(1)) - n*cos(x(1));
xPrime(3) = -1*n*x(2)*cos(x(1));
