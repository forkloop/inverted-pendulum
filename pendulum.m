% u = sat_{ng}(k(E-E_0)sign(\dot{\theta}}cos\theta))
% I will just use u = ng, assuming k is LARGE
% 
% status: angle, angle acceleration, normalized energy

x0 = [-pi, 0, -2];

global n theta

n = 1.5;
theta = -pi/2;

options = odeset('Events', @pevent);

[t, x, te, xe, ie] = ode45(@func, [0, 2], x0, options);

n = -1.5;
theta = -asin(2-2/abs(n));
options = odeset('Events', @pevent);
[t1, x1, te1, xe1, ie1] = ode45(@func, [0 5], xe, options);
%[t1, x1] = ode45(@func, [0 5], xe);

n = 0;
theta = 0;
options = odeset('Events', @pevent);
[t2, x2, te2, xe2, ie2] = ode45(@func, [0 5], xe1, options);


% plot angle
plot(t, x(:, 1));
hold on;
plot(t(end)+t1, x1(:, 1));
hold on;
plot(t(end)+t1(end)+t2, x2(:, 1));