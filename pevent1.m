function [lookfor stop direction] = pevent1(t, x)

theta = -pi/6;

lookfor = x(1)-theta;
stop = 1;
direction = 1;
