function xPrime = func(t, x)

global n
% n = 0;

xPrime = zeros(3, 1);
xPrime(1) = x(2);
xPrime(2) = sin(x(1)) + n*sign(x(2)*cos(x(1)))*cos(x(1));
xPrime(3) = n*sign(x(2)*cos(x(1)))*x(2)*cos(x(1));
