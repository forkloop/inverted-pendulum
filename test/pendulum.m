% u = sat_{ng}(k(E-E_0)sign(\dot{\theta}}cos\theta))
% I will just use u = ng, assuming k is LARGE
% 
% status: angle, angle acceleration, normalized energy

x0 = [-pi, 1e-3, -2];

global n theta k

k = 3;
n = 0.388;

%theta0 = -pi/2*k;
%[theta, fval] = fsolve(@solveTheta, theta0);
theta = -pi+0.3702;

options = odeset('Events', @pevent);

[t, x, te, xe, ie] = ode45(@func, [0, 20], x0, options);

n = -1.5;
theta = -asin(2-2/abs(n));
options = odeset('Events', @pevent);
[t1, x1, te1, xe1, ie1] = ode45(@func, [0 5], xe, options);
%[t1, x1] = ode45(@func, [0 5], xe);

n = 0;
theta = 0;
options = odeset('Events', @pevent);
[t2, x2, te2, xe2, ie2] = ode45(@func, [0 5], xe1, options);


% plot angle
plot(t, x(:, 1));
hold on;
plot(t(end)+t1, x1(:, 1));
hold on;
plot(t(end)+t1(end)+t2, x2(:, 1));