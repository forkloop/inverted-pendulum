function f = solveTheta(x)

global k

f = 2*sin(x) - cos(5*x) - 1;