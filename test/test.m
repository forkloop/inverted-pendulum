t = linspace(0, 10, 1000);
t_len = length(t);

theta = zeros(t_len, 1);
energy = zeros(t_len, 1);

theta(1) = -pi;
ddTheta = 0;
dTheta = 0;
energy(1) = -2;

% n = -1.5*sign(cos(theta)*dTheta);
n = 1.5;

for i=2:t_len
    theta(i) = t(i)*t(i)*(sin(theta(i-1))-n*cos(theta(i-1)))/2 - pi;
    ndTheta = theta(i) - theta(i-1);
    ddTheta = ndTheta - dTheta;
    energy(i) = ddTheta/2 + cos(theta(i)) -1;
    dTheta = ndTheta;
    n = -1.5*sign(cos(theta(i))*dTheta);
    if (abs(energy(i))<1e-3)
        break;
    end
end